# Eagle Libs

EDA libraries created using Autodesk Eagle software with [SnapEDA.com](https://snapeda.com) portal in mind for easy format conversion.

## What formats are supported by SnapEDA?

SnapEDA supports Altium, Eagle, KiCad, Cadence Allegro & OrCAD, PADS, and Pulsonix. They also provide 3D STEP models.
